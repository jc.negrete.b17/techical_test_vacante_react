"use client"
import React, { createContext, useContext, useState, ReactNode, useReducer } from 'react';
import { IFormData } from '../interface/formData';


interface FormContextValue {
  formData: IFormData;
  updateFormData: (newData: IFormData) => void;
}

const FormContext = createContext<FormContextValue | undefined>(undefined);

export function useFormContext(): FormContextValue {
  const context = useContext(FormContext);
  if (context === undefined) {
    throw new Error('useFormContext debe ser usado dentro de un FormContextProvider');
  }
  return context;
}

interface FormContextProviderProps {
  children: ReactNode;
}

export function FormContextProvider({ children }: FormContextProviderProps): JSX.Element {
  const [formData, setFormData] = useState<IFormData>({
    name: '',
    lastname: '',
    email: '',
    phoneNumber: '',
    message: '',
  });


  const updateFormData = (newData: IFormData) => {
    console.log('updateFormData', newData);
    setFormData(newData);
  };


  return (
    <FormContext.Provider value={{ formData, updateFormData }}>
      {children}
    </FormContext.Provider>
  );
}
