export interface IFormData {
  name: string;
  lastname: string;
  email: string;
  phoneNumber: string;
  message: string;
  
}