"use client";

import { SubmitErrorHandler, SubmitHandler, useForm } from "react-hook-form";
import Button from '@mui/material/Button';
import { TextField } from "@mui/material";
import { useState } from "react";
import { useFormContext } from "../core/context/formContext";
import { IFormData } from "../core/interface/formData";


export default function Home() {
  const { formData, updateFormData } = useFormContext();
  
  const {
    handleSubmit,
    register,
    formState: { errors },
    reset
  } = useForm<IFormData>();

  const [buttonText, setButtonText] = useState('Enviar');

  const handleInputChange = (e: any) => {
    const { name, value } = e.target;
    updateFormData({ ...formData, [name]: value });
  };

  const onSubmit: SubmitHandler<IFormData> = async (data) => {
    setButtonText("Enviando...");
     await fetch('../core/mailController/mail.js', {
        method: 'POST',
        body: JSON.stringify({...data}),
      }).then(() => {
        console.log("email enviado")
        console.log(data)
      }).catch ((error)=> {
      console.error(error);
    })
    reset()
    setButtonText("Enviar")
    
  }


  const sendEmail = (data: IFormData) => {
    updateFormData(data)
    setTimeout(() => {
      onSubmit(formData)
    }, 2000);
  }

  const onError: SubmitErrorHandler<FormData> = (errors) => console.log(errors);

  return (
    <div className="isolate bg-white px-6 py-2 sm:py-4 lg:px-8">
      <div className="mx-auto max-w-2xl text-center">
        <h2 className="text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl">
          Formulario de Contacto
        </h2>
      </div>
      <form
        onSubmit={handleSubmit(sendEmail, onError)}
        className="mx-auto mt-16 max-w-xl sm:mt-20"
      >
        <div className="grid grid-cols-1 gap-x-8 gap-y-6 sm:grid-cols-2">
            <div className="mt-2.5">
              <TextField 
                id="outlined-basic" 
                label="Nombre" 
                variant="outlined" 
              {...register("name", {
                required: "Nombre es requerido.",
                minLength: {
                  value: 3,
                  message: "Nombre debe tener por lo menos 3 caracteres",
                },
                onChange: handleInputChange
              })}
              />
              {errors?.name && (
                <span className="text-red-700">{errors.name.message}</span>
              )}
            </div>
          <div>
            <div className="mt-2.5">
               <TextField 
                id="outlined-basic" 
                label="Apellido" 
                variant="outlined" 
              {...register("lastname", {
                required: "Apellido es requerido.",
                minLength: {
                  value: 3,
                  message: "Apellido debe tener por lo menos 3 caracteres",
                },
                onChange: handleInputChange
              })}
              />
              {errors?.lastname && (
                <span className="text-red-700">{errors.lastname.message}</span>
              )}
            </div>
          </div>
          <div className="sm:col-span-2">
            <div className="mt-2.5">
              <TextField 
                fullWidth 
                type="email"
                id="outlined-basic" 
                label="Email" 
                variant="outlined" 
                className="block w-full rounded-md border-0  text-gray-900 shadow-sm ring-1  ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"

              {...register("email", {
                required: "Email es requerido.",
                onChange: handleInputChange


              })}
              />
              {errors?.email && (
                <span className="text-red-700">{errors.email.message}</span>
              )}
            </div>
          </div>
          <div className="sm:col-span-2">
            <TextField 
                fullWidth 
                type="tel"
                id="outlined-basic" 
                label="Telefono" 
                variant="outlined" 
                className="block w-full rounded-md border-0  text-gray-900 shadow-sm ring-1  ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"

              {...register("phoneNumber", {
                required: "Telefono es requerido.",
                onChange: handleInputChange

              })}
              />
              {errors?.phoneNumber && (
                <span className="text-red-700">{errors.phoneNumber.message}</span>
              )}
          </div>
          <div className="sm:col-span-2">
            <div className="mt-2.5">
              <TextField
              fullWidth
              id="outlined-multiline-flexible"
              label="Mensaje"
              multiline
              minRows={4}
              maxRows={4}
              {...register("message", {
                onChange: handleInputChange
              })}
          />
            </div>
          </div>
        </div>
        <div className="mt-10">
          <Button
            type="submit"
            className="block w-full rounded-md bg-indigo-600 px-3.5 py-2.5 text-center text-sm font-semibold text-white shadow-sm hover:bg-indigo-400 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-500"
          >
            <span>{buttonText}</span>
          </Button>
        </div>
      </form>
    </div>
  );
}
